#include "extcode.h"

#define API extern "C" __declspec(dllexport)

#include <scTDC.h>
#include <memory>
#include <cstdint>

namespace {


class Ref
{
public:
  Ref(uint32_t, uint32_t, uint32_t, uint32_t, uint32_t);
  ~Ref();
  int dd;
  int histo_pd;
  int image_pd;
  double binsize;
  const uint32_t t_binning;
  const size_t X;
  const size_t Y;
  const size_t T;
  unsigned *last_image;
  unsigned *last_histo;
};

Ref::Ref
(uint32_t image_size_x,
uint32_t image_size_y,
uint32_t image_size_t,
uint32_t time_binning,
uint32_t size_histo)
    :dd(-1),
    histo_pd(-1),
    image_pd(-1),
    binsize(0),
    t_binning(time_binning),
    X(image_size_x),
    Y(image_size_y),
    T(size_histo),
    last_image(NULL),
    last_histo(NULL)
{
  int ret = sc_tdc_init_inifile("tdc_gpx3.ini");
  dd = ret;
  if (ret < 0) {
    DbgPrintf("tdc init failed with %d code\n", ret);
    goto err1;
  }


  ret = sc_tdc_get_binsize2(dd, &binsize);
  if (ret < 0) {
    DbgPrintf("get binsize failed with %d code\n", ret);
    goto err2;
  }

  struct sc_pipe_dld_image_xy_params_t params_xy;
  params_xy.channel = -1;
  params_xy.modulo = 0;
  params_xy.binning.x = 1U;
  params_xy.binning.y = 1U;
  params_xy.binning.time = 1ULL;
  params_xy.roi.offset.x = 0;
  params_xy.roi.offset.y = 0;
  params_xy.roi.offset.time = 0;
  params_xy.roi.size.x = X;
  params_xy.roi.size.y = Y;
  params_xy.roi.size.time = (unsigned long long)(
    image_size_t/(1e-6*binsize));

  params_xy.accumulation_ms = 0;
  params_xy.allocator_owner = NULL;
  params_xy.allocator_cb = NULL;

  ret = sc_pipe_open2(dd, DLD_IMAGE_XY, static_cast <void *> (&params_xy));
  image_pd = ret;
  if (ret < 0) {
    DbgPrintf("open pipe (image) failed with %d code\n", ret);
    goto err3;
  }


  struct sc_pipe_dld_sum_histo_params_t params_sh;
  params_sh.channel = -1;
  params_sh.modulo = 0;
  params_sh.binning.x = 1U;
  params_sh.binning.y = 1U;
  params_sh.binning.time = t_binning;
  params_sh.roi.offset.x = 0;
  params_sh.roi.offset.y = 0;
  params_sh.roi.offset.time = 0;
  params_sh.roi.size.x = X;
  params_sh.roi.size.y = Y;
  params_sh.roi.size.time = T;
  params_sh.accumulation_ms = 0;
  params_sh.allocator_owner = NULL;
  params_sh.allocator_cb = NULL;

  ret = sc_pipe_open2(dd, DLD_SUM_HISTO, static_cast <void *> (&params_sh));
  histo_pd = ret;
  if (ret < 0) {
    DbgPrintf("open pipe (histo) failed with %d code\n", ret);
    goto err4;
  }

  return;

err4:
  sc_pipe_close2(dd, image_pd);
err3:
err2:
  sc_tdc_deinit2(dd);
err1:
  throw int(ret);
}

Ref::~Ref()
{
  sc_pipe_close2(dd, histo_pd);
  sc_pipe_close2(dd, image_pd);
  sc_tdc_deinit2(dd);
}


} //namespace

API Ref *tdc_init
(uint32_t size_x,
uint32_t size_y,
uint32_t image_size_t,
uint32_t time_binning,
uint32_t histosize,
double *binsize);
API int tdc_start_measure(Ref *r, long mt);
API int tdc_get_last_image(Ref *r, unsigned long *image, long image_size);
API int tdc_get_last_histo(Ref *r, double *histo, long histo_size);
API void tdc_fini(Ref *r);

API void tdc_fini(Ref *r)
{
  delete r;
};

API Ref *tdc_init
(uint32_t size_x,
uint32_t size_y,
uint32_t image_size_t,
uint32_t time_binning,
uint32_t histosize,
double *binsize)
{
  try {
    Ref *r = new Ref(size_x, size_y, image_size_t, time_binning, histosize);
    *binsize = r->binsize;
    return r;
  } catch (int &) {
    return 0;
  }
}

API int tdc_start_measure(Ref *r, long mt)
{
  int ret;

  ret = sc_tdc_start_measure2(r->dd, mt);
  if (ret < 0) {
    DbgPrintf("start_measure2 failed with %d code\n", ret);
    goto err1;
  }

  ret = sc_pipe_read2(r->dd, r->image_pd, &(r->last_image), mt + 3000);
  if (ret < 0) {
    DbgPrintf("pipe_read2 (image) failed with %d code\n", ret);
    goto err2;
  }

  ret = sc_pipe_read2(r->dd, r->histo_pd, &(r->last_histo), mt + 3000);
  if (ret < 0) {
    DbgPrintf("pipe_read2 (histo) failed with %d code\n", ret);
    goto err3;
  }

  return 0;

err3:
  r->last_image = NULL;
err2:
err1:
  return ret;
}

API int tdc_get_last_image(Ref *r, unsigned long *image, long image_size)
{
  if (image_size != (r->X)*(r->Y)) {
    DbgPrintf("array for image has wrong size %d, %llu %llu\n",
      image_size, r->X, r->Y);
    return -1;
  }

  for(long i=0; i<image_size; ++i) {
    image[i] = (r->last_image)[i];
  }

  return 0;
}

API int tdc_get_last_histo(Ref *r, double *histo, long histo_size)
{
  if (histo_size != r->T) {
    DbgPrintf("array for histo has wrong size\n");
    return -1;
  }

  for(long i=0; i<histo_size; ++i) {
    histo[i] = (r->last_histo)[i] * (r->binsize);
  }

  return 0;
}
