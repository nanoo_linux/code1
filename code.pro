
CONFIG += do_debug
do_debug {
	CONFIG += debug
	CONFIG -= release
  DEFINES += DEBUG
} else {
	CONFIG -= debug
	CONFIG += release
}

VER_MAJ = 1
VER_MIN = 0
VER_PAT = 2

VERSION = $${VER_MAJ}.$${VER_MIN}.$${VER_PAT}

TEMPLATE = lib
CONFIG -= qt

DEFINES -= UNICODE
DEFINES += _CRT_SECURE_NO_WARNINGS

QMAKE_TARGET_COMPANY = UniMainz Physics Institute
INCLUDEPATH += libs

LIBS += libs/scTDC1.lib
LIBS += libs/pthreadVC2.lib


#CONFIG += no_lv
!no_lv {
  INCLUDEPATH += "c:/Program Files/National Instruments/LabVIEW 8.2/cintools/"
  LIBS += user32.lib
  LIBS += "c:/Program Files/National Instruments/LabVIEW 8.2/cintools/labview.lib"
}

OBJECTS_DIR	= obj
MOC_DIR = moc

SOURCES += code.cpp

#dependency:
# pthread: >= 2.9.1
